<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SocialAccount;
use App\User;
use Socialite;
use Auth;

class SocialAccountController extends Controller
{
    public function redirectToProvider($provider){
        // call the socialite redirect method
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider){
            
            try{
            $user=Socialite::driver($provider)->user();

            }catch(Exception $e){
                return redirect('/login');
            }

            //this method finds or create the user
            $authUser= $this->findOrCreateUser($user,$provider);
            

            //dd($authUser);
            //exit;
            
            // now login the user
            Auth::login($authUser);

            
            //dd($authUser);
            // now redirect to the appropriate page
            return redirect('/home');
    
        }


        // function to find out if the user info return from the social site
        // exits or not
        public function findOrCreateUser($socialUser,$provider){
            $account=SocialAccount::where('provider_name',$provider)->where('provider_id',$socialUser->getId())->first();
            //dd($account);
           // exit;
            if($account){
                return $account->user;
            }else{
                $user=User::where('email',$socialUser->getEmail())->first();
            
                // now if user with the email from the social site does not exist
                if(! $user ){
                    // create the user and the corresponding social account
                    $user=User::create(
                        [
                            'email'=>$socialUser->getEmail(),
                            'name'=>$socialUser->getName()
                        ]
                    );
                }

                $user->accounts()->create([
                    'provider_name'=>$provider,
                    'provider_id'=>$socialUser->getId(),
                ]);
            
                return $user;
            }
        }
}
