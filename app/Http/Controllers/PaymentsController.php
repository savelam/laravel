<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class PaymentsController extends Controller
{
    public function pay(Request $request,$plan){

        $user= Auth::user();
        if($user->subscription('primary')){
            $user->subscription('primary')->swap($plan);
        }else{
           // $user->newSubscription('primary',$plan)->create($request->stripeToken); // normal subscription
            //$user->newSubscription('primary',$plan)->trialDays(14)->create($request->stripeToken); //subscription with trial days
            
            // subscrition with coupon
            $user->newSubscription('primary',$plan)->trialDays(14)->withCoupon('10off')->create($request->stripeToken);
            
        }

       
        return redirect("/home");
    }

    // function to cancel subscription

    public function cancel(){
        $user=Auth::user();
        $user->subscription('primary')->cancel();

        return  redirect("/home");
    }
}
