@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

         @if(Auth::user()->subscribed('primary') && Auth::user()->subscription('primary')->onGracePeriod())
                    <div class="alert alert-danger">
                    Your subscription will not renew. You have canceled,
                    but still have pre-paid time on your subscription
                    </div>
                    @endif

                     @if(Auth::user()->subscribed('primary') && Auth::user()->subscription('primary')->onTrial())
                    <div class="alert alert-info">
                    Your subscription is on 14 days trial
                    </div>
                    @endif
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h4>Manage your subcriptions</h4>



                        @if(Auth::user()->subscribed('primary'))
                            <p class="lead">
                            You are subscribed!!
                            </p>
                            @if(!Auth::user()->subscription('primary')->onGracePeriod())
                             @if(Auth::user()->subscribedToPlan('monthly','primary'))
                             <p class="lead">
                            You are currently subscribed to monthly plan
                            </p>
                             <a href="/pay/yearly" class="btn btn-primary btn-lg">Upgrade to Annual</a>
                             @endif

                            @if(Auth::user()->subscribedToPlan('yearly','primary'))
                            <p class="lead">
                            You are currently subscribed to yearly plan
                            </p>
                            <a href="/pay/monthly" class="btn btn-primary btn-lg">Downgrade to Monthly</a>
                            @endif

                            <a href="{{route('cancel')}}" class="btn btn-danger btn-lg">Cancel Plan</a>

                           @endif


                            @else
                            <p class="lead">
                            You are not yet subscribed!
                            </p>
                            <h5>Subscribe</h5>
                            <form action="/pay/monthly" method="POST">
                    {{ csrf_field() }}
                        <script
                            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                            data-key="{{ env('STRIPE_KEY')}}"
                            data-amount="1000"
                            data-name="Cashier Inc."
                            data-description="Subscribe monthly"
                            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                            data-locale="auto"
                            data-label="Monthly Subscription"
                            data-panel-label="Subscribe"
                            >
                        </script>
                        </form>

                        <form action="/pay/yearly" method="POST" style="margin-top: 10px;">
                        {{ csrf_field() }}
                        <script
                            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                            data-key="{{ env('STRIPE_KEY')}}"
                            data-amount="10000"
                            data-name="Cashier Inc."
                            data-description="Subscribe Yearly"
                            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                            data-locale="auto"
                            data-label="Annual Subscription"
                            data-panel-label="Subscribe"
                            >
                        </script>
                        </form>

                        @endif





                </div>
            </div>
        </div>
    </div>
</div>
@endsection
